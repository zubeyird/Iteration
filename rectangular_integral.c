/**
 * Author: Zubeyir
 * Date:2021/02/13 
 * Source: Teacher Chris Bourke's book
 * Chapter 4: Loops
 * Aim:
 * 
 * This program a test for example 4.12
 * This program calculate area under its curve
 * For calculating uses method of rectangular method integral
 * Between [a,b] steps should be from h=(b-a)/n n=(b-a)/h
 * Function example of f(x)=sinc(x)
 * Step is 10k
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

//A Function prototype you desire
double equation(double x);

int main(int argc, char ** argv){

    
    double a=-10,b=10;
    

    double n=10000;
    double h=((b-a)/n);
    double counter=0;
    double sum=0;
    double step=a;

    /** This increasing cycle goes a->b  
     * an another word this goes left to right along x axis
     */

    while (b>(step))
    {   
        step+=h;
        if ((h*equation(step))<0)
        {
            sum +=((h*equation(step))*(-1));
        }else
        {
            sum +=(h*equation(step));
        }

        counter++;
    }

    printf("\nCurve f(x)=12-2x definite integration between [%lf,%lf]=%lf\n",a,b,sum);
    printf("It copleted in %f step \n",counter);
    

return 0;

}


double equation(double x){
    double y=sin(x)/x;
    return y;
}