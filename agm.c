/**
 * Author: Zubeyir
 * Date:2021/02/06 
 * Source: Teacher Chris Bourke's book
 * Chapter 4: Loops
 * Aim:
 * Solving 4.11. Exercise in book
 * This program computes aritmetic- geometric mean of an array!\
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>


int main(int argc, char ** argv){

int n;


printf("Welcome, This program computes aritmetic- geometric mean of an array!\n");
printf("\nEnter how much number you have:");
scanf("%d",&n); 


double numbers[n];
for (int i = 0; i < n; i++)
{
    printf("\nEnter numbers:");
    scanf("%lf",&numbers[i]);
}



int size=sizeof(numbers)/sizeof(numbers[0]);
double sum;


  
for (int i = 0; i < size; i++)
{   
sum+=numbers[i];
}

double aritmetic_mean=sum/size;
double mult=1;

for (int i = 0; i < size; i++)
{
mult*=numbers[i];
}

double geometric_m=pow(mult,1.0/size);



printf("\n Aritmetic mean of %lf,%lf = %lf",numbers[0],numbers[1],aritmetic_mean);
printf("\n Geometric mean of %lf,%lf = %lf\n",numbers[0],numbers[1],geometric_m);

/** a1=am Aritmetic mean 
 * g1=gm Geometric mean
 * This generalization need to undestand question
 * then can calculate iteration 
 */
int iteration_counter=0;
while(fabs(aritmetic_mean-geometric_m)>0.000000001){
    aritmetic_mean=0.5*(geometric_m+aritmetic_mean);
    geometric_m=sqrt(geometric_m*aritmetic_mean);
    iteration_counter++;
}
  

 
printf(" a(n+1)= %lf\n",aritmetic_mean);
printf(" g(n+1)= %lf\n",geometric_m);
printf("İteration count:%d\n",iteration_counter);

return 0;
}
